## Update 2024-01-25

This was reported to Mesa here: https://gitlab.freedesktop.org/mesa/mesa/-/issues/10481

A fix was committed here: https://gitlab.freedesktop.org/mesa/mesa/-/merge_requests/27197

The fix released as part of Mesa 23.3.4: https://docs.mesa3d.org/relnotes/23.3.4.html

## Vector Buffer Reference Bug

This is a minimal reproduction project to demonstrate incorrect behaviour I've been facing using Vulkan and the GLSL GL_EXT_Buffer_Reference extension.

The code performs a very simple operation in a compute shader, by writing and then reading a vec4 from a buffer through a buffer_reference function parameter.

The result can be seen by looking at the output of DebugPrintfEXT when executing the program.
Note: The output should be made visible to stdout thanks to the included vk_layer_settings.txt, but for some reason this doesn't actually work for me. You may have to run vkconfig and select the Validation / Debug Printf preset to see it.

The expected output (as verified on Linux with NVIDIA proprietary driver) is:

`x=11, y=22, z=33, w=44`

The erroneous output seen on Linux AMD, Intel and llvmpipe drivers is:

`x=11, y=11, z=11, w=11`

## Usage

On Linux, running `./compile.sh` followed by `./VectorBufferReferenceTest` should compile the shader and program, then run the program.
If no output is seen when running the program, you may need to run vkconfig and select the Validation / Debug Printf preset, then run again.

If you have more than one physical device available in Vulkan, you can change this line in `VectorBufferReferenceBug.c` to select another index:
```
#define PHYSICAL_DEVICE_INDEX 0
```
Note that nearly no error checking is done at all, I was just trying to get to the smallest program that would demonstrate the issue.

This hasn't been tested on Windows yet either.

## Additional notes

This is not specific to `debugPrintfEXT` output, it does also happen when returning the value out of the function, or writing it out to a buffer.
It only seems to happen on single-component reads occurring right after a vector write. Any operations in-between that interact with the vec4 seem to "fix" the behaviour.
It does not seem to occur when reading the full vec4 or another swizzle, so `.yz` correctly returns `22, 33` in this case.
It does not seem to occur if the write is done before the function call.

My current suspicion is a SPIRV compiler optimizer bug somehow related to Mesa, where the write followed by a component read is being shortcut incorrectly, but I haven't looked deep enough to confirm this yet.

Any input welcome ! I'd love to get to the bottom of this.

