#include <stdio.h>
#include <stdlib.h>
#include <vulkan/vulkan.h>

#define PHYSICAL_DEVICE_INDEX 0

void PrintVkResult(const char*Text, int Result)
{
	if (Result != VK_SUCCESS)
	{
		printf("Result of %s: %d.\n", Text, Result);
	}
}

void ReadFileContents(const char* in_Filename, char** out_ReturnedContents, size_t* out_ReturnedContentsSize)
{
	*out_ReturnedContents = NULL;
	*out_ReturnedContentsSize = 0;

	FILE* pFile = fopen(in_Filename, "rb");
	fseek(pFile, 0, SEEK_END);
	int32_t FileSize = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);
	*out_ReturnedContents = malloc(FileSize);
	fread(*out_ReturnedContents, 1, FileSize, pFile);
	fclose(pFile);

	*out_ReturnedContentsSize = FileSize;
}

VkResult FindBufferMemoryType(const VkPhysicalDevice in_PhysicalDevice, uint32_t in_TypeFilter, VkMemoryPropertyFlags in_Properties, uint32_t* out_MemoryType)
{
	*out_MemoryType = 0;
	VkPhysicalDeviceMemoryProperties PhysicalDeviceMemoryProperties;
	vkGetPhysicalDeviceMemoryProperties(in_PhysicalDevice, &PhysicalDeviceMemoryProperties);
	for (uint32_t PhysicalDeviceMemoryPropertyIndex = 0; PhysicalDeviceMemoryPropertyIndex < PhysicalDeviceMemoryProperties.memoryTypeCount; ++PhysicalDeviceMemoryPropertyIndex)
	{
		if (in_TypeFilter & (1 << PhysicalDeviceMemoryPropertyIndex) && (PhysicalDeviceMemoryProperties.memoryTypes[PhysicalDeviceMemoryPropertyIndex].propertyFlags & in_Properties) == in_Properties)
		{
			*out_MemoryType = PhysicalDeviceMemoryPropertyIndex;
			return VK_SUCCESS;
		}
	}

	return VK_ERROR_INITIALIZATION_FAILED;
}

int main(int argc, char** argv)
{
	VkInstance Instance;

	VkApplicationInfo appInfo = { 0 };
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.apiVersion = VK_API_VERSION_1_3;

	VkInstanceCreateInfo createInfo = { 0 };
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	VkResult Result = vkCreateInstance(&createInfo, NULL, &Instance);
	PrintVkResult("vkCreateInstance", Result);
	
	VkPhysicalDevice PhysicalDevices[10] = { 0 };
	uint32_t DeviceCount = sizeof(PhysicalDevices)/sizeof(*PhysicalDevices);
	vkEnumeratePhysicalDevices(Instance, &DeviceCount, PhysicalDevices);
	VkPhysicalDevice PhysicalDevice = PhysicalDevices[PHYSICAL_DEVICE_INDEX];

	VkPhysicalDeviceProperties PhysicalDeviceProperties;
	vkGetPhysicalDeviceProperties(PhysicalDevice, &PhysicalDeviceProperties);
	printf("Selected device: Device #%d (%s, API=%d, DriverVersion=%u, Vendor=%d, DeviceID=%d, Type=%d).\n", PHYSICAL_DEVICE_INDEX, PhysicalDeviceProperties.deviceName, PhysicalDeviceProperties.apiVersion, PhysicalDeviceProperties.driverVersion, PhysicalDeviceProperties.vendorID, PhysicalDeviceProperties.deviceID, PhysicalDeviceProperties.deviceType);

	int ComputeFamily = -1;

	VkQueueFamilyProperties QueueFamilies[10];
	uint32_t QueueFamilyCount = sizeof(QueueFamilies) / sizeof(*QueueFamilies);
	vkGetPhysicalDeviceQueueFamilyProperties(PhysicalDevice, &QueueFamilyCount, QueueFamilies);

	for (uint32_t QueueFamilyIndex = 0; QueueFamilyIndex < QueueFamilyCount; ++QueueFamilyIndex)
	{
		const VkQueueFamilyProperties* FamilyProps = &QueueFamilies[QueueFamilyIndex];
		int bCompute = FamilyProps->queueFlags & VK_QUEUE_COMPUTE_BIT;
		if (bCompute)
		{
			ComputeFamily = QueueFamilyIndex;
		}
	}

	float QueuePriority = 1.0f;
	VkDeviceQueueCreateInfo DeviceQueueCreateInfo = { 0 };
	DeviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	DeviceQueueCreateInfo.queueFamilyIndex = ComputeFamily;
	DeviceQueueCreateInfo.queueCount = 1;
	DeviceQueueCreateInfo.pQueuePriorities = &QueuePriority;

	VkDeviceCreateInfo DeviceCreateInfo = { 0 };
	DeviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	DeviceCreateInfo.pQueueCreateInfos = &DeviceQueueCreateInfo;
	DeviceCreateInfo.queueCreateInfoCount = 1;
	
	VkPhysicalDeviceVulkan12Features Vulkan12Features = { };
	Vulkan12Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
	Vulkan12Features.bufferDeviceAddress = VK_TRUE;
	Vulkan12Features.pNext = (void*)DeviceCreateInfo.pNext;
	DeviceCreateInfo.pNext = &Vulkan12Features;

	VkDevice Device;
	Result = vkCreateDevice(PhysicalDevice, &DeviceCreateInfo, NULL, &Device);
	PrintVkResult("vkCreateDevice", Result);
	
	VkQueue ComputeQueue;
	vkGetDeviceQueue(Device, ComputeFamily, 0, &ComputeQueue);

	VkCommandPoolCreateInfo CommandPoolCreateInfo = { 0 };
	CommandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	CommandPoolCreateInfo.queueFamilyIndex = ComputeFamily;
	CommandPoolCreateInfo.flags = 0;

	VkCommandPool CommandPool;
	Result = vkCreateCommandPool(Device, &CommandPoolCreateInfo, NULL, &CommandPool);
	PrintVkResult("vkCreateCommandPool", Result);

	VkCommandBufferAllocateInfo CommandBufferAllocateInfo = { 0 };
	CommandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	CommandBufferAllocateInfo.commandPool = CommandPool;
	CommandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	CommandBufferAllocateInfo.commandBufferCount = 1;

	VkCommandBuffer CommandBuffer;
	Result = vkAllocateCommandBuffers(Device, &CommandBufferAllocateInfo, &CommandBuffer);
	PrintVkResult("vkAllocateCommandBuffers", Result);

	VkCommandBufferBeginInfo ComputeCommandBufferBeginInfo = { 0 };
	ComputeCommandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	vkBeginCommandBuffer(CommandBuffer, &ComputeCommandBufferBeginInfo);

	VkShaderModule ComputeShaderModule = VK_NULL_HANDLE;

	char* ShaderData;
	size_t ShaderSize;
	ReadFileContents("shader.spv", &ShaderData, &ShaderSize);

	VkShaderModuleCreateInfo ShaderModuleCreateInfo = { 0 };
	ShaderModuleCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	ShaderModuleCreateInfo.codeSize = ShaderSize;
	ShaderModuleCreateInfo.pCode = (uint32_t*)ShaderData;

	Result = vkCreateShaderModule(Device, &ShaderModuleCreateInfo, NULL, &ComputeShaderModule);
	PrintVkResult("vkCreateShaderModule", Result);

	VkPipelineShaderStageCreateInfo ComputePipelineShaderStageCreateInfo = { 0 };
	ComputePipelineShaderStageCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	ComputePipelineShaderStageCreateInfo.stage = VK_SHADER_STAGE_COMPUTE_BIT;
	ComputePipelineShaderStageCreateInfo.module = ComputeShaderModule;
	ComputePipelineShaderStageCreateInfo.pName = "main";

	VkDescriptorSetLayoutCreateInfo DescriptorSetLayoutCreateInfo = { 0 };
	DescriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	DescriptorSetLayoutCreateInfo.bindingCount = 0;
	DescriptorSetLayoutCreateInfo.pBindings = NULL;

	VkDescriptorSetLayout DescriptorSetLayout = VK_NULL_HANDLE;
	Result = vkCreateDescriptorSetLayout(Device, &DescriptorSetLayoutCreateInfo, NULL, &DescriptorSetLayout);
	PrintVkResult("vkCreateDescriptorSetLayout", Result);

	VkDescriptorSetLayout DescriptorSetLayouts[] = { DescriptorSetLayout };

	VkPushConstantRange PushConstantRange = { 0 };
	PushConstantRange.stageFlags = VK_SHADER_STAGE_COMPUTE_BIT;
	PushConstantRange.offset = 0;
	PushConstantRange.size = 8;

	VkPipelineLayoutCreateInfo PipelineLayoutCreateInfo = { 0 };
	PipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	PipelineLayoutCreateInfo.setLayoutCount = 1;
	PipelineLayoutCreateInfo.pSetLayouts = DescriptorSetLayouts;
	PipelineLayoutCreateInfo.pushConstantRangeCount = 1;
	PipelineLayoutCreateInfo.pPushConstantRanges = &PushConstantRange;

	VkPipelineLayout PipelineLayout = VK_NULL_HANDLE;
	Result = vkCreatePipelineLayout(Device, &PipelineLayoutCreateInfo, NULL, &PipelineLayout);
	PrintVkResult("vkCreatePipelineLayout", Result);

	VkComputePipelineCreateInfo ComputePipelineCreateInfo = { 0 };
	ComputePipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
	ComputePipelineCreateInfo.layout = PipelineLayout;
	ComputePipelineCreateInfo.stage = ComputePipelineShaderStageCreateInfo;
	ComputePipelineCreateInfo.basePipelineHandle = VK_NULL_HANDLE;
	ComputePipelineCreateInfo.basePipelineIndex = -1;

	VkPipeline ComputePipeline;
	Result = vkCreateComputePipelines(Device, NULL, 1, &ComputePipelineCreateInfo, NULL, &ComputePipeline);
	PrintVkResult("vkCreateComputePipelines", Result);

	VkBufferCreateInfo BufferCreateInfo = { 0 };
	BufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	BufferCreateInfo.size = 16;
	BufferCreateInfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
	BufferCreateInfo.flags = 0;
	BufferCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	VkBuffer DeviceBuffer = VK_NULL_HANDLE;
	Result = vkCreateBuffer(Device, &BufferCreateInfo, NULL, &DeviceBuffer);
	PrintVkResult("vkCreateBuffer", Result);

	VkMemoryRequirements MemoryRequirements = { 0 };
	vkGetBufferMemoryRequirements(Device, DeviceBuffer, &MemoryRequirements);

	uint32_t MemoryTypeIndex;
	Result = FindBufferMemoryType(PhysicalDevice, MemoryRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &MemoryTypeIndex);
	PrintVkResult("FindMemoryType", Result);

	VkMemoryAllocateInfo MemoryAllocateInfo = { 0 };
	MemoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	MemoryAllocateInfo.allocationSize = MemoryRequirements.size;
	MemoryAllocateInfo.memoryTypeIndex = MemoryTypeIndex;

	VkMemoryAllocateFlagsInfo Flags = { };
	Flags.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_FLAGS_INFO;
	Flags.flags = VK_MEMORY_ALLOCATE_DEVICE_ADDRESS_BIT;
	MemoryAllocateInfo.pNext = &Flags;

	VkDeviceMemory DeviceBufferMemory = VK_NULL_HANDLE;
	Result = vkAllocateMemory(Device, &MemoryAllocateInfo, NULL, &DeviceBufferMemory);
	PrintVkResult("vkAllocateMemory", Result);

	vkBindBufferMemory(Device, DeviceBuffer, DeviceBufferMemory, 0);

	VkBufferDeviceAddressInfo AddressInfo = { };
	AddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
	AddressInfo.buffer = DeviceBuffer;
	VkDeviceAddress BufferAddress = vkGetBufferDeviceAddress(Device, &AddressInfo);

	uint64_t PushConstantValue[] = { BufferAddress };

	vkCmdPushConstants(CommandBuffer, PipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, 8, PushConstantValue);
	vkCmdBindPipeline(CommandBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, ComputePipeline);
	vkCmdDispatch(CommandBuffer, 1, 1, 1);

	vkEndCommandBuffer(CommandBuffer);

	VkSubmitInfo SubmitInfo = { 0 };
	SubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	SubmitInfo.commandBufferCount = 1;
	SubmitInfo.pCommandBuffers = &CommandBuffer;

	printf("Executing compute shader...\n");

	Result = vkQueueSubmit(ComputeQueue, 1, &SubmitInfo, NULL);
	PrintVkResult("vkQueueSubmit", Result);

	Result = vkQueueWaitIdle(ComputeQueue);
	PrintVkResult("vkQueueWaitIdle", Result);
	printf("Done. If no shader output was displayed, open vkconfig with the Validation / Debug Printf Preset.\n");

	return 0;
}
